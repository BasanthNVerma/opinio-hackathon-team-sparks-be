package com.venturesity.rest;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.venturesity.domain.FullRoute;
import com.venturesity.service.HomeServiceImpl;

/**
 * 
 * 
 * @author shreesha
 *
 *         This class has all the REST end-points consumed by our iOS client
 */
@RestController
public class HomeController {

	@Autowired
	HomeServiceImpl homeService;

	private static final Logger log = LoggerFactory.getLogger(HomeController.class);

	/**
	 * This method takes an object of user shared route , compares
	 * with other routes existing in the database and finds an optimal solution
	 * based on parameters distance,time
	 * and saves it to database
	 * 
	 * @param fullRoute
	 * @return ResponseEntity<JSONObject> - Returns the "distance" and "time" in
	 *         JSON
	 *         required to commute for the given input
	 * @throws CustomGenericException
	 */
	@RequestMapping( value = "/saveDist", method = RequestMethod.POST )
	public ResponseEntity<JSONObject> saveDist( @RequestBody FullRoute fullRoute ) throws Exception
	{
		try
		{
			log.info("Endpoint -->/saveDist");
			return homeService.saveDist(fullRoute);
		}
		catch( Exception e )
		{
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * This method takes an existing order id and finds the route related to the
	 * order id,returns the optimised route saved in the database
	 *
	 * 
	 * 
	 * @param fullRoute
	 * @return ResponseEntity<JSONObject> - Returns the optimised route in JSON
	 *         having parameters like "start point","end point",
	 *         "lat's and long's of the intermediate nodes"
	 * @throws CustomGenericException
	 */
	@RequestMapping( value = "/getRoute/{orderId}", method = RequestMethod.GET )
	public ResponseEntity<JSONObject> getRoute( @PathVariable( "orderId" ) Long orderId) throws Exception
	{
		try
		{
			log.info("Endpoint -->/getRoute/{orderId}");
			return homeService.getRoute(orderId);
		}
		catch( Exception e )
		{
			e.printStackTrace();
			throw e;
		}
	}

}
