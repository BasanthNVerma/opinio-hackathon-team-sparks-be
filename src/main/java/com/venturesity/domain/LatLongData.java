package com.venturesity.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 
 * 
 * @author shreesha
 *	
 *	This POJO contains lat and long of a particular node which inturns belongs to "FullRoute" class
 */
@Entity
@Table( name = "lat_long_data" )
public class LatLongData {

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "lld_id" )
	private Integer id;

	@Column( name = "lld_latitude" )
	private Double nodeLatitude;

	@Column( name = "lld_longitude" )
	private Double nodeLongitude;


	public Integer getId()
	{
		return id;
	}

	public void setId( Integer id )
	{
		this.id = id;
	}

	public Double getNodeLatitude()
	{
		return nodeLatitude;
	}

	public void setNodeLatitude( Double nodeLatitude )
	{
		this.nodeLatitude = nodeLatitude;
	}

	public Double getNodeLongitude()
	{
		return nodeLongitude;
	}

	public void setNodeLongitude( Double nodeLongitude )
	{
		this.nodeLongitude = nodeLongitude;
	}

	/*public List<FullRoute> getFullRouteList()
	{
		return fullRouteList;
	}

	public void setFullRouteList( List<FullRoute> fullRouteList )
	{
		this.fullRouteList = fullRouteList;
	}*/

}
