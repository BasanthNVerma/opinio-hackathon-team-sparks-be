package com.venturesity.domain;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * 
 * 
 * @author shreesha
 *	
 *	This POJO contains the route shared by the user 
 */

@Entity
@Table( name = "full_route" )
public class FullRoute {

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "fr_id" )
	private Integer id;

	@Column( name = "fr_start_point_lat" )
	private Double startPointLat;

	@Column( name = "fr_start_point_long" )
	private Double startPointLong;

	@Column( name = "fr_end_point_lat" )
	private Double endPointLat;

	@Column( name = "fr_end_point_long" )
	private Double endPointLong;

	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="lld_frn_fr_id")
	//@JoinTable( name = "full_route_lat_long_data", joinColumns = {
			//@JoinColumn( name = "fr_id" ) }, inverseJoinColumns = { @JoinColumn( name = "lld_id" ) })
	private List<LatLongData> latLongDataList;

	@Column( name = "fr_order_d" )
	private Long orderId;

	@Column( name = "fr_is_customer_given" )
	private Boolean isCustomerGiven;

	public Integer getId()
	{
		return id;
	}

	public void setId( Integer id )
	{
		this.id = id;
	}

	public Double getStartPointLat()
	{
		return startPointLat;
	}

	public void setStartPointLat( Double startPointLat )
	{
		this.startPointLat = startPointLat;
	}

	public Double getStartPointLong()
	{
		return startPointLong;
	}

	public void setStartPointLong( Double startPointLong )
	{
		this.startPointLong = startPointLong;
	}

	public Double getEndPointLat()
	{
		return endPointLat;
	}

	public void setEndPointLat( Double endPointLat )
	{
		this.endPointLat = endPointLat;
	}

	public Double getEndPointLong()
	{
		return endPointLong;
	}

	public void setEndPointLong( Double endPointLong )
	{
		this.endPointLong = endPointLong;
	}

	public List<LatLongData> getLatLongDataList()
	{
		return latLongDataList;
	}

	public void setLatLongDataList( List<LatLongData> latLongDataList )
	{
		this.latLongDataList = latLongDataList;
	}

	public Long getOrderId()
	{
		return orderId;
	}

	public void setOrderId( Long orderId )
	{
		this.orderId = orderId;
	}

	public Boolean getIsCustomerGiven()
	{
		return isCustomerGiven;
	}

	public void setIsCustomerGiven( Boolean isCustomerGiven )
	{
		this.isCustomerGiven = isCustomerGiven;
	}

}
