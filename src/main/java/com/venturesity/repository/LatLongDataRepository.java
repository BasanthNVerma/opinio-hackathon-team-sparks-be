package com.venturesity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.venturesity.domain.LatLongData;


/**
 * 
 * 
 * @author shreesha
 *	
 *	Spring JPA Repository for LatLongData.class
 */
public interface LatLongDataRepository extends JpaRepository<LatLongData,Integer>{

}
