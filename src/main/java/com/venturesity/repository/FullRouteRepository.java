package com.venturesity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.venturesity.domain.FullRoute;

/**
 * 
 * 
 * @author shreesha
 *	
 *	Spring JPA Repository for FullRoute.class
 */
public interface FullRouteRepository extends JpaRepository<FullRoute,Integer>{

	FullRoute findByOrderId( Long orderId );

}
