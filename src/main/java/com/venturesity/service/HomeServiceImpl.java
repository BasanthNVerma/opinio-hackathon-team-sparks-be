package com.venturesity.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.venturesity.domain.FullRoute;
import com.venturesity.domain.LatLongData;
import com.venturesity.exception.CustomGenericException;
import com.venturesity.repository.FullRouteRepository;
import com.venturesity.utils.ElementData;

/**
 * 
 * 
 * @author shreesha
 * 
 *         This class deals with the business logic for the end points the app
 *         exposes
 */
@Service
@Transactional
public class HomeServiceImpl {

	private static final Logger log = LoggerFactory.getLogger(HomeServiceImpl.class);

	@Autowired
	FullRouteRepository fullRouteRepo;

	/**
	 * This method takes an object of user shared route , compares
	 * with other routes existing in the database and finds an optimal solution
	 * based on parameters distance,time
	 * and saves it to database
	 * 
	 * @param fullRoute
	 * @return ResponseEntity<JSONObject> - Returns the "distance" and "time" in
	 *         JSON
	 *         required to commute for the given input
	 * @throws CustomGenericException
	 * 
	 *             *This method is not completely validated*
	 */
	@SuppressWarnings( "unchecked" )
	public ResponseEntity<JSONObject> saveDist( FullRoute fullRoute ) throws Exception
	{
		JSONObject jsonObject = new JSONObject();
		try
		{
			if( fullRoute.getIsCustomerGiven() == null || fullRoute.getEndPointLat() == null
					|| fullRoute.getEndPointLong() == null || fullRoute.getOrderId() == null
					|| fullRoute.getStartPointLat() == null || fullRoute.getStartPointLong() == null )
			{
				throw new CustomGenericException("404", "Required parameters not found");
			}
			log.info("Saving route information");
			fullRoute = this.routeOptimizer(fullRoute);
			FullRoute fullRouteDomain = fullRouteRepo.save(this.nodeSorter(fullRoute));
			log.info("ROUTE OPTIMIZATION -->done");
			log.info("Saved optimized route data successfully");
			String API_KEY = "AIzaSyCOf2UR_1N6KOYXXK5sTn3fJjuUeWiY6mM";
			String origin = fullRouteDomain.getStartPointLat() + "," + fullRouteDomain.getStartPointLong();
			String dest = fullRouteDomain.getEndPointLat() + "," + fullRouteDomain.getEndPointLong();
			String url_request = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins="
					+ origin + "&destinations=" + dest + "&key=" + API_KEY;
			String response = this.requestMaker(url_request);
			System.out.println(response);

			// map string to json

			org.json.JSONObject mainObj = new org.json.JSONObject(response);
			org.json.JSONArray rowsArray = mainObj.getJSONArray("rows");
			ObjectMapper mapper = new ObjectMapper();
			for( int i = 0; i < rowsArray.length(); i++ )
			{
				org.json.JSONObject elementObj = rowsArray.getJSONObject(i);
				org.json.JSONArray elementArray = elementObj.getJSONArray("elements");
				for( int j = 0; j < elementArray.length(); j++ )
				{
					org.json.JSONObject duration = (org.json.JSONObject) elementArray.getJSONObject(i).get("duration");
					org.json.JSONObject distance = (org.json.JSONObject) elementArray.getJSONObject(i).get("distance");
					ElementData e = (ElementData) mapper.readValue(duration.toString(), ElementData.class);
					jsonObject.put("duration", e);
					e = (ElementData) mapper.readValue(distance.toString(), ElementData.class);
					jsonObject.put("distance", e);
				}
			}
			jsonObject.put("message", "successfully saved data");
		}
		catch( Exception e )
		{
			e.printStackTrace();
			throw e;
		}
		return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
	}

	/**
	 * This method takes an existing order id and finds the route related to the
	 * order id,returns the optimised route saved in the database
	 *
	 * 
	 * 
	 * @param fullRoute
	 * @return ResponseEntity<JSONObject> - Returns the optimised route in JSON
	 *         having parameters like "start point","end point",
	 *         "lat's and long's of the intermediate nodes"
	 * @throws CustomGenericException
	 */
	@SuppressWarnings( "unchecked" )
	public ResponseEntity<JSONObject> getRoute( Long orderId ) throws Exception
	{
		JSONObject jsonObject = new JSONObject();
		try
		{
			if( orderId == null )
			{
				throw new CustomGenericException("404", "Required parameters not found");
			}
			FullRoute fullRouteDomain = fullRouteRepo.findByOrderId(orderId);
			if( fullRouteDomain == null )
			{
				throw new CustomGenericException("404", "No route found for the order id");
			}
			jsonObject.put("optimized route", fullRouteDomain);
		}
		catch( Exception e )
		{
			e.printStackTrace();
			throw e;
		}
		return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
	}

	// *************************************************************************************************//
	// ***************ALL THE METHODS BELOW ARE USED AS UTILITIES FOR SERVING
	// THE REQUISITES************//

	/**
	 * 
	 * @param google
	 *            distance matrix api url
	 * @return returns the google's distance response json as string
	 * @throws IOException
	 */
	public String requestMaker( String url ) throws IOException
	{
		try
		{
			OkHttpClient client = new OkHttpClient();
			Request request = new Request.Builder().url(url).build();
			Response response = client.newCall(request).execute();
			return response.body().string();
		}
		catch( Exception e )
		{
			e.printStackTrace();
			throw e;
		}

	}

	/**
	 * 
	 * 
	 * @param currentLat
	 *            - Double
	 * @param currentLong
	 *            - Double
	 * @param dbLat
	 *            - Double
	 * @param dbLong
	 *            - Double
	 * @return Returns True - if the passed source and destination are within a
	 *         range of 1.2miles
	 *         False - if not
	 * @throws Exception
	 */
	public Boolean distanceChecker( Double currentLat, Double currentLong, Double dbLat, Double dbLong )
			throws Exception
	{
		try
		{
			if( currentLat == null || currentLong == null || dbLat == null || dbLong == null )
			{
				throw new CustomGenericException("404", "Required parameters not found");
			}
			String API_KEY = "AIzaSyCOf2UR_1N6KOYXXK5sTn3fJjuUeWiY6mM";
			String origin = currentLat + "," + currentLong;
			String dest = dbLat + "," + dbLong;
			String url_request = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins="
					+ origin + "&destinations=" + dest + "&key=" + API_KEY;
			String response = this.requestMaker(url_request);

			org.json.JSONObject mainObj = new org.json.JSONObject(response);
			org.json.JSONArray rowsArray = mainObj.getJSONArray("rows");
			ObjectMapper mapper = new ObjectMapper();
			ElementData e = null;
			for( int i = 0; i < rowsArray.length(); i++ )
			{
				org.json.JSONObject elementObj = rowsArray.getJSONObject(i);
				org.json.JSONArray elementArray = elementObj.getJSONArray("elements");
				for( int j = 0; j < elementArray.length(); j++ )
				{
					org.json.JSONObject distance = (org.json.JSONObject) elementArray.getJSONObject(i).get("distance");
					e = (ElementData) mapper.readValue(distance.toString(), ElementData.class);
				}
			}
			try
			{
				Integer distance = Integer
						.parseInt((e.getText().substring(0, e.getText().length() - 2).trim().replace(",", "")));
				log.info("Distance between given two places is :" + distance);
				if( distance <= 1.24274 )
					return true;
				else
					return false;
			}
			catch( Exception e1 )
			{
				log.info("Error caught since the distance between these two places is a fractional value");
				Float distance = Float
						.parseFloat((e.getText().substring(0, e.getText().length() - 2).trim().replace(",", "")));
				if( distance <= 1.24274 )
					return true;
				else
					return false;
			}

		}
		catch( Exception e )
		{
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 
	 * 
	 * @param fullRouteUI
	 * @return Returns the optimised route
	 * @throws Exception
	 * 
	 *             This method takes a route , searches in the database for a
	 *             similar route considering its nodes , if any node is found
	 *             similar checks for parameters like node size , travle
	 *             distance , travel time and returns the optimised "fullRoute"
	 *             object
	 */

	public FullRoute routeOptimizer( FullRoute fullRouteUI ) throws Exception
	{
		try
		{
			if( fullRouteUI.getIsCustomerGiven() == null || fullRouteUI.getEndPointLat() == null
					|| fullRouteUI.getEndPointLong() == null || fullRouteUI.getOrderId() == null
					|| fullRouteUI.getStartPointLat() == null || fullRouteUI.getStartPointLong() == null )
			{
				throw new CustomGenericException("404", "Required parameters not found");
			}
			List<FullRoute> list = fullRouteRepo.findAll();

			for( FullRoute fr : list )
			{
				if( fr.getOrderId() == fullRouteUI.getOrderId() )
					continue;
				Boolean nodeA = this.distanceChecker(fullRouteUI.getStartPointLat(), fullRouteUI.getStartPointLong(),
						fr.getStartPointLat(), fr.getStartPointLong());
				Boolean nodeB = this.distanceChecker(fullRouteUI.getEndPointLat(), fullRouteUI.getEndPointLong(),
						fr.getEndPointLat(), fr.getEndPointLong());
				if( nodeA && nodeB )
				{
					if( !fr.getLatLongDataList().isEmpty() && !fullRouteUI.getIsCustomerGiven()
							&& !fullRouteUI.getLatLongDataList().isEmpty() )
					{
						if( fullRouteUI.getLatLongDataList().size() > fr.getLatLongDataList().size() )
						{
							List<LatLongData> listLatLong = new ArrayList<>();
							fullRouteUI.getLatLongDataList().clear();
							for( LatLongData l : fr.getLatLongDataList() )
							{
								LatLongData newL = new LatLongData();
								newL.setNodeLatitude(l.getNodeLatitude());
								newL.setNodeLongitude(l.getNodeLongitude());
								listLatLong.add(newL);
							}
							fullRouteUI.setLatLongDataList(listLatLong);
							log.info("(nodes replaced)");
							return fullRouteUI;
							/*
							 * jsonObject.put("optimized-route", fullRouteUI);
							 */

						}
					}
					else if( fullRouteUI.getLatLongDataList().isEmpty() && !fr.getLatLongDataList().isEmpty() )
					{
						List<LatLongData> listLatLongg = new ArrayList<>();
						for( LatLongData l : fr.getLatLongDataList() )
						{
							LatLongData newL = new LatLongData();
							newL.setNodeLatitude(l.getNodeLatitude());
							newL.setNodeLongitude(l.getNodeLongitude());
							listLatLongg.add(newL);
						}
						fullRouteUI.setLatLongDataList(listLatLongg);
						// jsonObject.put("optimized-route", fullRouteUI);
						log.info("(nodes copied)");
						return fullRouteUI;
					}
				}

			}
			log.info("returning the ui object back with no modifications");
			return fullRouteUI;
		}
		catch( Exception e )
		{
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 
	 * 
	 * @param fullRoute
	 * @return Returns the input in the same way but sorts the intermediatory
	 *         nodes in ascending oder taking source code as the compare point
	 * @throws Exception
	 */
	public FullRoute nodeSorter( FullRoute fullRoute ) throws Exception
	{
		try
		{
			if( fullRoute.getIsCustomerGiven() == null || fullRoute.getEndPointLat() == null
					|| fullRoute.getEndPointLong() == null || fullRoute.getOrderId() == null
					|| fullRoute.getStartPointLat() == null || fullRoute.getStartPointLong() == null )
			{
				throw new CustomGenericException("404", "Required parameters not found");
			}
			Map<String, Double> sortingMap = new HashMap<>();
			LinkedHashMap<String, Double> sortedMap = new LinkedHashMap<>();
			String API_KEY = "AIzaSyCOf2UR_1N6KOYXXK5sTn3fJjuUeWiY6mM";
			if( fullRoute.getLatLongDataList().isEmpty() )
			{
				return fullRoute;
			}
			for( LatLongData l : fullRoute.getLatLongDataList() )
			{

				String origin = fullRoute.getStartPointLat() + "," + fullRoute.getStartPointLong();
				String dest = l.getNodeLatitude() + "," + l.getNodeLongitude();
				String url_request = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins="
						+ origin + "&destinations=" + dest + "&key=" + API_KEY;
				String response = this.requestMaker(url_request);

				org.json.JSONObject mainObj = new org.json.JSONObject(response);
				org.json.JSONArray rowsArray = mainObj.getJSONArray("rows");
				ObjectMapper mapper = new ObjectMapper();
				ElementData e = null;
				for( int i = 0; i < rowsArray.length(); i++ )
				{
					org.json.JSONObject elementObj = rowsArray.getJSONObject(i);
					org.json.JSONArray elementArray = elementObj.getJSONArray("elements");
					for( int j = 0; j < elementArray.length(); j++ )
					{
						org.json.JSONObject distance = (org.json.JSONObject) elementArray.getJSONObject(i)
								.get("distance");
						e = (ElementData) mapper.readValue(distance.toString(), ElementData.class);
					}
				}
				try
				{
					String distance = (e.getText().substring(0, e.getText().length() - 2).trim().replace(",", ""));
					log.info("Distance between given two places is :" + distance);
					sortingMap.put(dest, Double.valueOf(distance));
				}
				catch( Exception e1 )
				{
					log.info("Error caught since the distance between these two places is a fractional value");
					String distance = (e.getText().substring(0, e.getText().length() - 2).trim().replace(",", ""));
					sortingMap.put(dest, Double.valueOf(distance));
				}
				List<String> mapKeys = new ArrayList<>(sortingMap.keySet());
				List<Double> mapValues = new ArrayList<>(sortingMap.values());
				Collections.sort(mapValues);
				Collections.sort(mapKeys);

				Iterator<Double> valueIt = mapValues.iterator();
				while( valueIt.hasNext() )
				{
					Double val = valueIt.next();
					Iterator<String> keyIt = mapKeys.iterator();

					while( keyIt.hasNext() )
					{
						String key = keyIt.next();
						Double comp1 = sortingMap.get(key);
						Double comp2 = val;

						if( comp1.equals(comp2) )
						{
							keyIt.remove();
							sortedMap.put(key, val);
							break;
						}
					}
				}

			}
			List<LatLongData> sortedLatLong = new ArrayList<>();
			for( String s : sortedMap.keySet() )
			{
				String[] arr = s.split(",");
				Double lat = Double.valueOf(arr[0]);
				Double longi = Double.valueOf(arr[1]);
				LatLongData newLatLong = new LatLongData();
				newLatLong.setNodeLatitude(lat);
				newLatLong.setNodeLongitude(longi);
				sortedLatLong.add(newLatLong);
			}
			fullRoute.getLatLongDataList().clear();
			fullRoute.setLatLongDataList(sortedLatLong);
			return fullRoute;
		}
		catch( Exception e )
		{
			e.printStackTrace();
			throw e;
		}
	}

}
